# Support Hours Purchasing - 1
| Hours Purchased | Putchased On | Expires On |
| ------ | ------ | ------ |
| 5 | 2021-09-01 | 2021-09-29 |

| Issue Id | Logger | Spent Time | Date | Note |
| ------ | ------ | ------ | ------ | ------ |

# Support Hours Purchasing - 2
| Hours Purchased | Putchased On | Expires On |
| ------ | ------ | ------ |
| 10 | 2022-01-01 | 2022-01-31 |

| Issue Id | Logger | Spent Time | Date | Note |
| ------ | ------ | ------ | ------ | ------ |
| 1 | Rushdi Mohamed | 2.0 | 2021-11-16 | [View](https://gitlab.com/rushdi-testing-org/testing-2/-/issues/1) |
| 2 | Rushdi Mohamed | 2.0 | 2021-11-17 | [View](https://gitlab.com/rushdi-testing-org/testing-2/-/issues/1) |
| 3 | Rushdi Mohamed | 5.0 | 2021-11-24 | [View](https://gitlab.com/rushdi-testing-org/testing-2/-/issues/1) |

# Support Hours Purchasing - 3
| Hours Purchased | Putchased On | Expires On |
| ------ | ------ | ------ |
| 10 | 2022-02-01 | 2022-02-28 |

| Issue Id | Logger | Spent Time | Date | Note |
| ------ | ------ | ------ | ------ | ------ |

