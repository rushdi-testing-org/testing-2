# README

# Project 31352137 Support Utilization

<div align="right">

# [View Utilization History](https://gitlab.com/rushdi-testing-org/testing-2/-/blob/main/support-utilization.md)
</div>

```mermaid
	pie
			"Used Hours":9.0
			"Remaining Hours":10
```

<div align="left">

# Total Purchased Support Hours 25
</div>
<div align="left">

## Remaining Support hours 10
## Expires on 2022-02-28
</div>


# General Support Guidelines

Entgra Support uses GitLab for issue tracking. You can create a new issue by clicking [New Issue](/../../issues/new).

## Issue types

You need to assign relevant label to issue based on severity of the issue. This label help Entgra support team to prioritize the issues and provide solutions under SLA.

### Severity Level Guide
#### Catastrophic (Level 1)
A catastrophic incident is a production problem, which may severely impact the client's production systems, or in which the client's production systems are down or not functioning; and no procedural workaround exists.
During a catastrophic incident,
- All or a substantial portion of the client's mission critical data is at a significant risk of loss or corruption.
- The client have had a substantial loss of service.
- The client's business operations have been severely disrupted.
Client resources should be available and willing to work on a 24x7 basis with Entgra to resolve the Incident. Examples include a complete loss of service, production systems that have crashed, or a production system that hangs indefinitely.

#### Urgent (Level 2)
An urgent incident is a problem where the client's system is functioning, but in a severely reduced capacity. The system is exposed to potential loss or interruption of service.

- Operations can continue in a restricted fashion, although long-term productivity might be adversely affected.
- A major milestone is at risk. Ongoing and incremental installations are affected.
- A temporary workaround is available.


#### Non Blocker (Level 3)
A Non Blocker is a medium-to-low impact problem, which involves partial, non-critical functionality loss.

- Impaired operations of some components, but allows the user to continue using the software.
- Initial installation milestones are at minimal risk.

This may be a minor issue with limited loss, or no loss of functionality, or impact to the client's operation and issues in which there is an easy circumvention or avoidance by the end user.


#### Query
A question you need to raise in order to clarify something or get instructions or advices from Entgra team.


## Issue lifecycle

Entgra support maintain issue lifecycle with Gitlab labels. Here is the general issue lifecycle;

```mermaid
graph TD;
		A(Open)-->B(Waiting for Entgra);
		B(Waiting on Entgra)-->C(In progress);
		C(In progress)-->D(Waiting on Client);
		D(Waiting on Client)--Issue is not solved-->B(Waiting on Entgra);
		D(Waiting on Client)--Issue is solved-->E(Fixed);
```
